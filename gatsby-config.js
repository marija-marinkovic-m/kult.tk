const path = require('path');

const decodeHtmlNumbers = str =>
  str.replace(/&#(\d+);/g, function(_, number) {
    return String.fromCharCode(number);
  });

module.exports = {
  siteMetadata: {
    title: 'Kult',
    description: 'Magazin za kulturu življenja',
    siteUrl: 'https://kult.tk', // full path to blog - no ending slash
  },
  plugins: [
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'content',
        path: path.join(__dirname, 'src', 'content'),
      },
    },
    'gatsby-plugin-emotion',
    'gatsby-plugin-typescript',
    'gatsby-transformer-sharp',
    'gatsby-plugin-react-helmet',
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-plugin-feed',

      options: {
        query: `
            {
              site {
                siteMetadata {
                  title
                  description
                  siteUrl
                  site_url: siteUrl
                }
              }
            }
          `,
        feeds: [
          {
            serialize: ({ query: { site, allWordpressPost } }) => {
              return allWordpressPost.edges.map(edge => {
                return Object.assign({}, edge.node.frontmatter, {
                  description: edge.node.excerpt,
                  date: edge.node.date,
                  url: site.siteMetadata.siteUrl + edge.node.slug,
                  guid: site.siteMetadata.siteUrl + edge.node.slug,
                  custom_elements: [{ 'content:encoded': edge.node.html }],
                });
              });
            },
            query: `
                {
                  allWordpressPost(
                    limit: 1000
                  ) {
                    edges {
                      node {
                        title
                        date(formatString: "MMMM DD")
                        slug
                        excerpt
                      }
                    }
                  }
                }
              `,
            output: '/rss.xml',
            title: 'Kult.tk RSS Feed',
          },
        ],
      },
    },
    {
      resolve: 'gatsby-plugin-postcss',
      options: {
        postCssPlugins: [require('postcss-color-function'), require('cssnano')()],
      },
    },
    {
      resolve: 'gatsby-source-wordpress',
      options: {
        baseUrl: 'dev-kult.pantheonsite.io',
        protocol: 'http',
        hostingWPCOM: false,
        useACF: true,
        verboseOutput: true,
        perPage: 100,
        auth: {},
        excludedRoutes: ['/*/*/comments', '/yoast/**', '/*/users/me', '/oembed/*'],
        normalizer: function({ entities }) {
          return entities.map(e => {
            if (e.title) {
              e.title = decodeHtmlNumbers(e.title);
            }
            if (e.name) {
              e.name = decodeHtmlNumbers(e.name);
            }
            return e;
          });
        },
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-119278210-2',
        // Puts tracking script in the head instead of the body
        head: false,
        anonymize: true,
      },
    },
    {
      resolve: 'gatsby-plugin-mailchimp',
      options: {
        endpoint:
          'https://kult.us19.list-manage.com/subscribe/post?u=508e36db4e9ccde2f8ac6fba0&amp;id=07c53b704e',
      },
    },
  ],
};
