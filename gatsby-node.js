const _ = require('lodash');
const path = require('path');
const slash = require('slash');
const { paginate } = require('gatsby-awesome-pagination');

const getOnlyPublished = edges => _.filter(edges, ({ node }) => node.status === 'publish');

exports.createPages = ({ graphql, actions: { createPage } }) => {
  // --- PAGES
  return (
    graphql(`
      {
        allWordpressPage {
          edges {
            node {
              id
              slug
              status
              template
            }
          }
        }
      }
    `)
      .then(result => {
        if (result.errors) {
          result.errors.forEach(e => console.error(e.toString()));
          return Promise.reject(result.errors);
        }

        const pageTemplate = path.resolve('./src/templates/page.tsx');

        // Only publish pages with a `status === 'publish'` in production. This
        // excludes drafts, future posts, etc. They will appear in development,
        // but not in a production build.

        const allPages = result.data.allWordpressPage.edges;
        const pages = process.env.NODE_ENV === 'production' ? getOnlyPublished(allPages) : allPages;

        _.each(pages, ({ node: page }) => {
          // Each page is required to have a `path` as well
          // as a template component. The `context` is
          // optional but is often necessary so the template
          // can query data specific to each page.
          createPage({
            path: `/${page.slug}/`,
            component: slash(pageTemplate),
            context: {
              id: page.id,
            },
          });
        });
      })

      // --- POSTS
      .then(() => {
        return graphql(`
          {
            allWordpressPost {
              edges {
                node {
                  id
                  slug
                  status
                }
              }
            }
          }
        `).then(result => {
          if (result.errors) {
            result.errors.forEach(e => console.error(e.toString()));
            return Promise.reject(result.errors);
          }
          const postTemplate = path.resolve(`./src/templates/post.tsx`);
          const blogTemplate = path.resolve(`./src/templates/blog.tsx`);

          // In production builds, filter for only published posts.
          const allPosts = result.data.allWordpressPost.edges;
          const posts =
            process.env.NODE_ENV === 'production' ? getOnlyPublished(allPosts) : allPosts;

          // Iterate over the array of posts
          _.each(posts, ({ node: post }) => {
            // Create the Gatsby page for this WordPress post
            createPage({
              path: `/${post.slug}/`,
              component: postTemplate,
              context: {
                id: post.id,
              },
            });
          });

          // Create a paginated blog, e.g., /, /page/2, /page/3
          paginate({
            createPage,
            items: posts,
            itemsPerPage: 10,
            pathPrefix: ({ pageNumber }) => (pageNumber === 0 ? `/` : `/page`),
            component: blogTemplate,
          });
        });
      })

      // --- AUTHORS
      .then(() => {
        return graphql(`
          {
            allWordpressWpUsers {
              edges {
                node {
                  id
                  slug
                }
              }
            }
          }
        `);
      })
      .then(result => {
        if (result.errors) {
          result.errors.forEach(e => console.error(e.toString()));
          return Promise.reject(result.errors);
        }

        const authorTemplate = path.resolve('./src/templates/author.tsx');

        _.each(result.data.allWordpressWpUsers.edges, ({ node: author }) => {
          createPage({
            path: `/author/${author.slug}`,
            component: authorTemplate,
            context: {
              id: author.id,
            },
          });
        });
      })

      // --- CATEGORIES
      .then(() => {
        return graphql(`
          {
            allWordpressCategory(filter: { count: { gt: 0 } }) {
              edges {
                node {
                  id
                  name
                  slug
                }
              }
            }
          }
        `);
      })
      .then(result => {
        if (result.errors) {
          result.errors.forEach(e => console.error(e.toString()));
          return Promise.reject(result.errors);
        }

        const categoriesTemplate = path.resolve(`./src/templates/category.tsx`);

        // Create a Gatsby page for each WordPress Category
        _.each(result.data.allWordpressCategory.edges, ({ node: cat }) => {
          createPage({
            path: `/categories/${cat.slug}/`,
            component: categoriesTemplate,
            context: {
              name: cat.name,
              slug: cat.slug,
            },
          });
        });
      })

      // --- TAGS
      .then(() => {
        return graphql(`
          {
            allWordpressTag(filter: { count: { gt: 0 } }) {
              edges {
                node {
                  id
                  name
                  slug
                }
              }
            }
          }
        `);
      })
      .then(result => {
        if (result.errors) {
          result.errors.forEach(e => console.error(e.toString()));
          return Promise.reject(result.errors);
        }

        const tagsTemplate = path.resolve(`./src/templates/tag.tsx`);

        // Create a Gatsby page for each WordPress tag
        _.each(result.data.allWordpressTag.edges, ({ node: tag }) => {
          createPage({
            path: `/tags/${tag.slug}/`,
            component: tagsTemplate,
            context: {
              name: tag.name,
              slug: tag.slug,
            },
          });
        });
      })
  );
};
