import React, { SFC } from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';
import { PageLayout } from '../layouts';
import PostList from '../components/PostList';
import { Post } from '../content/models';
import PageHgroup from '../components/PageHgroup';

interface Props {
  data: {
    site: { siteMetadata: { title: string } };
    wordpressWpUsers: {
      name: string;
      authored_wordpress__POST: Post[];
    };
    heroImage: { childImageSharp: { fluid: any } };
  };
}

const Author: SFC<Props> = props => {
  const { data } = props;
  const { authored_wordpress__POST, name } = data.wordpressWpUsers;
  const { title: siteTitle } = data.site.siteMetadata;

  // The `authored_wordpress__POST` returns a simple array instead of an array
  // of edges / nodes. We therefore need to convert the array here.
  const posts = authored_wordpress__POST.map(post => ({
    node: post,
  }));

  return (
    <PageLayout>
      <Helmet title={`${name} | ${siteTitle}`} />
      <PageHgroup title={name} bgr={props.data.heroImage.childImageSharp} />
      <PostList posts={posts} title={name} />
    </PageLayout>
  );
};

Author.defaultProps = {};
export default Author;

export const pageQuery = graphql`
  query AuthorPage($id: String!) {
    site {
      siteMetadata {
        title
      }
    }
    wordpressWpUsers(id: { eq: $id }) {
      name
      authored_wordpress__POST {
        ...PostListFields
      }
    }
    heroImage: file(relativePath: { eq: "img/blog-cover.jpg" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;
