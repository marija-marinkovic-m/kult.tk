import React, { SFC } from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';
import { PageLayout } from '../layouts';
import PostList from '../components/PostList';
import { Post } from '../content/models';
import PageHgroup from '../components/PageHgroup';

interface Props {
  data: {
    site: {
      siteMetadata: {
        title: string;
      };
    };
    allWordpressPost: {
      totalCount: number;
      edges: { node: Post }[];
    };
    heroImage: { childImageSharp: { fluid: any } };
  };
  pageContext: {
    currentPage: number;
    numPages: number;
    previousPagePath: string;
    nextPagePath: string;
    name: any;
  };
}

const Tag: SFC<Props> = props => {
  const { data, pageContext } = props;
  const { edges: posts } = data.allWordpressPost;
  const { title: siteTitle } = data.site.siteMetadata;
  const { name: tag } = pageContext;

  return (
    <PageLayout>
      <Helmet title={`${tag} | ${siteTitle}`} />
      <PageHgroup title={'Oznaka: ' + tag} bgr={props.data.heroImage.childImageSharp} />
      <PostList posts={posts} />
    </PageLayout>
  );
};

Tag.defaultProps = {};
export default Tag;

export const pageQuery = graphql`
  query TagPage($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    heroImage: file(relativePath: { eq: "img/blog-cover.jpg" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    allWordpressPost(filter: { tags: { elemMatch: { slug: { eq: $slug } } } }) {
      totalCount
      edges {
        node {
          ...PostListFields
        }
      }
    }
  }
`;
