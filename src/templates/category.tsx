import React, { SFC } from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';
import { PageLayout } from '../layouts';
import PostList from '../components/PostList';
import { Post } from '../content/models';
import PageHgroup from '../components/PageHgroup';

interface Props {
  data: {
    site: {
      siteMetadata: {
        title: string;
      };
    };
    allWordpressPost: {
      totalCount: number;
      edges: { node: Post }[];
    };
  };
  pageContext: {
    currentPage: number;
    numPages: number;
    previousPagePath: string;
    nextPagePath: string;
    name: any;
  };
}

const Category: SFC<Props> = props => {
  const { data, pageContext } = props;
  const { edges: posts } = data.allWordpressPost;
  const { title: siteTitle } = data.site.siteMetadata;
  const { name: category } = pageContext;
  return (
    <PageLayout>
      <Helmet title={`${category} | ${siteTitle}`} />
      <PageHgroup title={category} theme="light" />
      <PostList posts={posts} />
    </PageLayout>
  );
};

Category.defaultProps = {};
export default Category;

export const pageQuery = graphql`
  query CategoryPage($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    allWordpressPost(filter: { categories: { elemMatch: { slug: { eq: $slug } } } }) {
      totalCount
      edges {
        node {
          ...PostListFields
        }
      }
    }
  }
`;
