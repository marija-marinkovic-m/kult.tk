import React from 'react';
import { graphql } from 'gatsby';

import { PageLayout } from '../layouts';
import Helmet from 'react-helmet';
import { Page } from '../content/models';
import config from '../website-config';
import PostContent from '../components/PostContent';

import PageHgroup from '../components/PageHgroup';

interface Props {
  data: {
    wordpressPage: Page;
    heroImage: { childImageSharp: { fluid: any } };
  };
  pathContext: { slug: string };
}

export default ({
  data: {
    wordpressPage: page,
    heroImage: { childImageSharp },
  },
  pathContext,
}: Props) => {
  return (
    <PageLayout className="post-template">
      <Helmet>
        <title>{page.title}</title>
        <meta property="og:site_name" content={config.title} />
        <meta property="og:type" content="article" />
        <meta property="og:title" content={page.title} />
        <meta property="og:description" content={page.excerpt} />
        <meta property="og:url" content={config.siteUrl + pathContext.slug} />

        <meta property="article:publisher" content={config.facebook} />
        <meta property="article:author" content={config.facebook} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content={page.title} />
        <meta name="twitter:description" content={page.excerpt} />
        <meta name="twitter:url" content={config.siteUrl + pathContext.slug} />
      </Helmet>
      <PageHgroup title={page.title} bgr={childImageSharp} />
      <PostContent content={page.content} />
    </PageLayout>
  );
};

export const query = graphql`
  query($id: String!) {
    heroImage: file(relativePath: { eq: "img/blog-cover.jpg" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    wordpressPage(id: { eq: $id }) {
      id
      title
      content
      date(formatString: "MMMM DD, YYYY")
    }
  }
`;
