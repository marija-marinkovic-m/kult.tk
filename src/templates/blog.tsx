import React, { Component } from 'react';
import { graphql } from 'gatsby';
import { PageLayout } from '../layouts';

import PostList from '../components/PostList';
import Pagination from '../components/Pagination';
import { Post } from '../content/models';
import PageHgroup from '../components/PageHgroup';
import Helmet from 'react-helmet';
import config from '../website-config';

interface Props {
  data: {
    allWordpressPost: {
      edges: { node: Post }[];
    };
  };
  pageContext: {
    currentPage: number;
    numPages: number;
    previousPagePath: string;
    nextPagePath: string;
  };
}
export default class IndexPage extends Component<Props, any> {
  render() {
    const {
      pageContext,
      data: {
        allWordpressPost: { edges: posts },
      },
    } = this.props;
    return (
      <PageLayout>
        <Helmet title={`Članci | ${config.title} - ${config.description}`} />
        <PageHgroup title="Članci" theme="light" />
        <PostList posts={posts} title="Najnovije" />
        <Pagination pageContext={pageContext} />
      </PageLayout>
    );
  }
}

export const query = graphql`
  query IndexQuery($limit: Int!, $skip: Int!) {
    allWordpressPost(sort: { fields: date, order: DESC }, limit: $limit, skip: $skip) {
      edges {
        node {
          ...PostListFields
        }
      }
    }
  }
`;
