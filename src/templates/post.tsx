import React, { Component } from 'react';
import { graphql, Link } from 'gatsby';
import { Helmet } from 'react-helmet';
import styled, { css } from 'react-emotion';
import { setLightness } from 'polished';
import { has } from 'lodash';
import Img from 'gatsby-image';

import Layout from '../layouts';
import { Post } from '../content/models';
import { colors } from '../styles/colors';

import config from '../website-config';
import { SiteHeader, outer, inner, SiteMain } from '../styles/shared';
import SiteNav from '../components/header/SiteNav';
import PostContent from '../components/PostContent';
import Subscribe from '../components/subsribe/Subscribe';
import PostFullFooter from '../components/PostFullFooter';
import AuthorCard from '../components/AuthorCard';
import PostFullFooterRight from '../components/PostFullFooterRight';
import Footer from '../components/Footer';

import TagLinks from '../components/TagLinks';
import CategoryLinks from '../components/CategoryLinks';
// import ReadNextCard from '../components/ReadNextCard';

interface Props {
  data: { wordpressPost: Post };
  pathContext: { slug: string };
  logo: {
    childImageCharp: { fixed: any };
  };
  pageContext: {
    currentPage: number;
    numPages: number;
    previousPagePath: string;
    nextPagePath: string;
  };
}

const Wrapper = styled('div')({
  '.site-main': {
    background: '#fff',
    paddingBottom: '4vw',
  },
});

export const PostFull = css`
  position: relative;
  z-index: 50;
`;

export const NoImage = css`
  .post-full-content {
    padding-top: 0;
  }

  .post-full-content:before,
  .post-full-content:after {
    display: none;
  }
`;

export const PostFullHeader = styled.header`
  margin: 0 auto;
  padding: 6vw 3vw 3vw;
  max-width: 1040px;
  text-align: center;

  @media (max-width: 500px) {
    padding: 14vw 3vw 10vw;
  }
`;

const PostFullMeta = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${colors.midgrey};
  font-size: 1.4rem;
  font-weight: 600;
  text-transform: uppercase;

  @media (max-width: 500px) {
    font-size: 1.2rem;
    line-height: 1.3em;
  }
`;

const PostFullMetaDate = styled.time`
  color: ${colors.blue};
`;

export const PostFullTitle = styled.h1`
  margin: 0;
  color: ${setLightness('0.05', colors.darkgrey)};
  @media (max-width: 500px) {
    font-size: 2.9rem;
  }
`;

const PostFullImage = styled.figure`
  margin: 0 -10vw -165px;
  height: 800px;
  background: ${colors.lightgrey} center center;
  background-size: cover;
  border-radius: 5px;

  @media (max-width: 1170px) {
    margin: 0 -4vw -100px;
    height: 600px;
    border-radius: 0;
  }

  @media (max-width: 800px) {
    height: 400px;
  }
  @media (max-width: 500px) {
    margin-bottom: 4vw;
    height: 350px;
  }
`;

const DateDivider = styled.span`
  display: inline-block;
  margin: 0 6px 1px;
`;

// const ReadNextFeed = styled.div`
//   display: flex;
//   flex-wrap: wrap;
//   margin: 0 -20px;
//   padding: 40px 0 0 0;
// `;

const PostMeta = styled.div`
  margin-bottom: 60px;
  text-align: center;
`;

class PostTemplate extends Component<Props> {
  static defaultProps = {
    data: { wordpressPost: {} },
    edges: [],
  };
  render() {
    const {
      data: { wordpressPost: post },
      pathContext,
    } = this.props;

    let imageSrc = '';
    let width = '';
    let height = '';
    if (post.featured_media && post.featured_media.localFile) {
      imageSrc = post.featured_media.localFile.childImageSharp.fluid.src;
      width = post.featured_media.localFile.childImageSharp.fluid.sizes
        .split(', ')[1]
        .split('px')[0];
      height = String(
        Number(width) / post.featured_media.localFile.childImageSharp.fluid.aspectRatio,
      );
    }

    return (
      <Layout className="post-template">
        <Helmet>
          <title>
            {post.title} | {config.title}
          </title>

          <meta property="og:site_name" content={config.title} />
          <meta property="og:type" content="article" />
          <meta property="og:title" content={`${post.title} | ${config.title}`} />
          <meta property="og:description" content={post.excerpt} />
          <meta property="og:url" content={config.siteUrl + pathContext.slug} />

          <meta property="og:image" content={imageSrc} />
          <meta name="twitter:image" content={imageSrc} />

          <meta property="article:published_time" content={post.modified.toString()} />
          {has(post, 'tags[0]') && <meta property="article:tag" content={post.tags[0].name} />}

          <meta property="article:publisher" content={config.facebook} />
          <meta property="article:author" content={config.facebook} />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:title" content={post.title} />
          <meta name="twitter:description" content={post.excerpt} />
          <meta name="twitter:url" content={config.siteUrl + pathContext.slug} />

          <meta name="twitter:label1" content="Written by" />
          <meta name="twitter:data1" content={post.author.toString()} />
          <meta name="twitter:label2" content="Filed under" />
          {has(post, 'tags[0].name') && <meta name="twitter:data2" content={post.tags[0].name} />}

          {config.twitter && (
            <meta
              name="twitter:site"
              content={`@${config.twitter.split('https://twitter.com/')[0]}`}
            />
          )}
          {config.twitter && (
            <meta
              name="twitter:creator"
              content={`@${config.twitter.split('https://twitter.com/')[0]}`}
            />
          )}
          <meta property="og:image:width" content={width} />
          <meta property="og:image:height" content={height} />
        </Helmet>
        <Wrapper>
          <header className={`${SiteHeader} ${outer}`}>
            <div className={inner}>
              <SiteNav />
            </div>
          </header>
          <main id="site-main" className={`site-main ${SiteMain} ${outer}`}>
            <div className={`${inner}`}>
              <article className={`${PostFull} ${!post.featured_media ? NoImage : ''}`}>
                <PostFullHeader>
                  <PostFullMeta>
                    <PostFullMetaDate dateTime={post.modified.toString()}>
                      {post.modified}
                    </PostFullMetaDate>
                    {post.tags && post.tags.length > 0 && (
                      <>
                        <DateDivider>/</DateDivider>
                        <Link to={`/tags/${post.tags[0].slug}/`}>{post.tags[0].name}</Link>
                      </>
                    )}
                  </PostFullMeta>
                  <PostFullTitle>{post.title}</PostFullTitle>
                </PostFullHeader>

                {post.featured_media && (
                  <PostFullImage>
                    <Img
                      style={{ height: '100%' }}
                      fluid={post.featured_media.localFile.childImageSharp.fluid}
                    />
                  </PostFullImage>
                )}
                <PostContent content={post.content} />

                <PostMeta>
                  <TagLinks items={post.tags} />
                  <CategoryLinks items={post.categories} />
                </PostMeta>

                {/* The big email subscribe modal content */}
                {config.showSubscribe && <Subscribe title={config.title} />}

                <PostFullFooter>
                  <AuthorCard author={post.author} />
                  <PostFullFooterRight author={post.author} />
                </PostFullFooter>
              </article>
            </div>
          </main>

          {/* Links to Previous/Next posts */}
          {/* <aside className={`read-next ${outer}`}>
          <div className={`${inner}`}>
            <ReadNextFeed>
              {props.data.relatedPosts && (
                <ReadNextCard tags={post.frontmatter.tags} relatedPosts={props.data.relatedPosts} />
              )}

              {pageContext.prev && <PostCard post={props.pageContext.prev} />}
              {props.pageContext.next && <PostCard post={props.pageContext.next} />}
            </ReadNextFeed>
          </div>
        </aside> */}
          <Footer />
        </Wrapper>
      </Layout>
    );
  }
}
export default PostTemplate;

export const pageQuery = graphql`
  query($id: String!) {
    logo: file(relativePath: { eq: "img/kult-white.png" }) {
      childImageSharp {
        fixed {
          ...GatsbyImageSharpFixed
        }
      }
    }
    wordpressPost(id: { eq: $id }) {
      title
      content
      modified(formatString: "MMMM DD, YYYY")
      author {
        id
        name
        description
        slug
        avatar_urls {
          wordpress_48
        }
        acf {
          profilna_slika {
            localFile {
              childImageSharp {
                fluid(maxWidth: 48) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
      featured_media {
        localFile {
          childImageSharp {
            fluid(maxWidth: 1920) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
      categories {
        slug
        name
        count
      }
      tags {
        slug
        name
        count
      }
    }
  }
`;
