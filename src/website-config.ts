export interface WebsiteConfig {
  title: string;
  description: string;
  coverImage: string;
  logo: string;
  /**
   * blog full path, no ending slash!
   */
  siteUrl: string;
  facebook?: string;
  twitter?: string;
  /**
   * hide or show all email subscribe boxes
   */
  showSubscribe: boolean;
  /**
   * create a list on mailchimp and then create an embeddable signup form. this is the form action
   */
  mailchimpAction?: string;
  /**
   * this is the hidden input field name
   */
  mailchimpName?: string;
}

const config: WebsiteConfig = {
  title: 'Kult',
  description: 'Magazin za kulturu življenja',
  coverImage: 'img/blog-cover.jpg',
  logo: 'img/kult.png',
  siteUrl: 'https://kult.tk',
  facebook: 'https://www.facebook.com/kult-tk',
  twitter: 'https://twitter.com/kult-tk',
  showSubscribe: true,
};

export default config;
