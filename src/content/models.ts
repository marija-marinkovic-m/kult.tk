export interface Taxonomy {
  id: number;
  wordpress_id: number;
  name: string;
  slug: string;
  description: string;
  hierarchical: boolean;
  rest_base: string;
  types: string[];
}

export interface Category {
  id: number;
  wordpress_id: number;
  count: number;
  description: string;
  link: string;
  slug: string;
  name: string;
  taxonomy: Taxonomy;
}
export interface Tag {
  id: number;
  wordpress_id: number;
  count: number;
  description: string;
  link: string;
  name: string;
  slug: string;
  taxonomy: Taxonomy;
}

export interface Author {
  id: string;
  name: string;
  avatar_urls: {
    wordpress_24: string;
    wordpress_48: string;
    wordpress_96: string;
  };
  description: string;
  slug: string;
  acf?: {
    profilna_slika?: any;
  };
}
export interface WPMediaDetails {
  width: number;
  height: number;
  file: string;
}
export interface WPMedia {
  id: number;
  wordpress_id: number;
  date: Date;
  modified: Date;
  slug: string;
  status: string;
  type: string;
  title: string;
  author: number;
  description: string;
  caption: string;
  alt_text: string;
  media_type: string;
  mime_type: string;
  media_details: WPMediaDetails;
  post: number;
  source_url: string;
  localFile: { childImageSharp: { fluid: any; fixed: any } };
}

export interface Post {
  id: number;
  wordpress_id: number;
  date: Date;
  guid: string;
  modified: Date;
  slug: string;
  status: string;
  type: string;
  link: string;
  title: string;
  content: string;
  excerpt: string;
  author: Author;
  sticky: boolean;
  template: string;
  format: string;
  categories: Category[];
  tags: Tag[];
  featured_media: WPMedia;
}

export interface Page {
  id: number;
  wordpress_id: number;
  date: Date;
  guid: string;
  modified: Date;
  slug: string;
  status: string;
  type: string;
  link: string;
  title: string;
  content: string;
  excerpt: string;
  wordpress_parent: number;
  menu_order: number;
  template: string;
  author: Author;
}
