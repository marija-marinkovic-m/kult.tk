import React, { SFC } from 'react';
import { Link } from 'gatsby';
import styled from 'react-emotion';

import { Tag } from '../content/models';
import { colors } from '../styles/colors';

import TagIcon from '../components/icons/Tag';

interface Props {
  items: Tag[];
}

const Wrap = styled.span`
  display: block;
  margin-bottom: 4px;
  color: ${colors.midgrey};
  font-size: 1rem;
  line-height: 1.15em;
  font-weight: 500;
  letter-spacing: 0.5px;
  text-transform: uppercase;

  & > * {
    display: inline-block;
    margin-right: 5px;
  }
`;

const Icon = styled(TagIcon)`
  margin-right: 15px;
`;

const TagLinks: SFC<Props> = ({ items }) =>
  items && items.length > 0 ? (
    <Wrap>
      <Icon />
      {items.map(tag => (
        <Link key={tag.id} to={`/tags/${tag.slug}`}>
          {tag.name} ({tag.count})
        </Link>
      ))}
    </Wrap>
  ) : null;

TagLinks.defaultProps = {};
export default TagLinks;
