import React, { SFC } from 'react';
import { Link } from 'gatsby';
import styled from 'react-emotion';

import { Category } from '../content/models';
import { colors } from '../styles/colors';

import LabelIcon from '../components/icons/Label';

interface Props {
  items: Category[];
}

const Wrap = styled.span`
  display: block;
  margin-bottom: 4px;
  color: ${colors.midgrey};
  font-size: 1rem;
  line-height: 1.15em;
  font-weight: 500;
  letter-spacing: 0.5px;
  text-transform: uppercase;
  & > * {
    display: inline-block;
    margin-right: 5px;
  }
`;

const Icon = styled(LabelIcon)`
  margin-right: 15px;
`;

const CategoryLinks: SFC<Props> = ({ items }) =>
  items && items.length > 0 ? (
    <Wrap>
      <Icon />
      {items.map(category => (
        <Link key={category.id} to={`/categories/${category.slug}`}>
          {category.name} ({category.count})
        </Link>
      ))}
    </Wrap>
  ) : null;

CategoryLinks.defaultProps = {};
export default CategoryLinks;
