import { Link } from 'gatsby';
import Img from 'gatsby-image';
import React from 'react';

import styled, { css } from 'react-emotion';
import { colors } from '../styles/colors';
import { Post } from '../content/models';
import { lighten } from 'polished';
import readingTime from 'reading-time';

const PostCardStyles = css`
  flex: 1 1 300px;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  margin: 0 20px 40px;
  min-height: 300px;
  background: #fff center center;
  background-size: cover;
  border-radius: 5px;
  box-shadow: rgba(39, 44, 49, 0.06) 8px 14px 38px, rgba(39, 44, 49, 0.03) 1px 3px 8px;
  transition: all 0.5s ease;

  .post-card-image {
    filter: grayscale(100%);
    transition: filter 300ms ease-in;
  }

  :hover {
    box-shadow: rgba(39, 44, 49, 0.07) 8px 28px 50px, rgba(39, 44, 49, 0.04) 1px 6px 12px;
    transition: all 0.4s ease;
    transform: translate3D(0, -1px, 0) scale(1.02);

    /* .post-card-image {
      filter: grayscale(0);
    } */
  }
`;

const PostCardImageLink = css`
  position: relative;
  display: block;
  overflow: hidden;
  border-radius: 5px 5px 0 0;
`;

const PostCardImage = styled.div`
  width: auto;
  height: 200px;
  background: ${colors.lightgrey} no-repeat center center;
  background-size: cover;
`;

const PostCardContent = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const PostCardContentLink = css`
  position: relative;
  flex-grow: 1;
  display: block;
  padding: 25px 25px 0;
  color: ${colors.darkgrey};

  :hover {
    text-decoration: none;
  }
`;

const PostCardTitle = styled.h2`
  margin-top: 0;
  a {
    color: inherit;
  }
`;

const PostCardExcerpt = styled.section`
  font-family: Georgia, serif;
`;

const PostCardMeta = styled.footer`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  padding: 0 25px 25px;
`;

const AuthorList = styled.ul`
  display: flex;
  flex-wrap: wrap-reverse;
  margin: 0;
  padding: 0;
  list-style: none;
`;

const AuthorListItem = styled.li`
  position: relative;
  flex-shrink: 0;
  margin: 0;
  padding: 0;

  :nth-child(1) {
    z-index: 10;
  }
  :nth-child(2) {
    z-index: 9;
  }
  :nth-child(3) {
    z-index: 8;
  }
  :nth-child(4) {
    z-index: 7;
  }
  :nth-child(5) {
    z-index: 6;
  }
  :nth-child(6) {
    z-index: 5;
  }
  :nth-child(7) {
    z-index: 4;
  }
  :nth-child(8) {
    z-index: 3;
  }
  :nth-child(9) {
    z-index: 2;
  }
  :nth-child(10) {
    z-index: 1;
  }
  :hover .author-name-tooltip {
    opacity: 1;
    transform: translateY(0px);
  }
`;

const AuthorNameTooltip = styled.div`
  position: absolute;
  bottom: 105%;
  z-index: 999;
  display: block;
  padding: 2px 8px;
  color: white;
  font-size: 1.2rem;
  letter-spacing: 0.2px;
  white-space: nowrap;
  background: ${colors.darkgrey};
  border-radius: 3px;
  box-shadow: rgba(39, 44, 49, 0.08) 0 12px 26px, rgba(39, 44, 49, 0.03) 1px 3px 8px;
  opacity: 0;
  transition: all 0.3s cubic-bezier(0.4, 0.01, 0.165, 0.99);
  transform: translateY(6px);
  pointer-events: none;

  @media (max-width: 650px) {
    display: none;
  }
`;

const StaticAvatar = css`
  display: block;
  overflow: hidden;
  margin: 0 -5px;
  width: 34px;
  height: 34px;
  border: #fff 2px solid;
  border-radius: 100%;
`;

const AuthorProfileImage = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  background: color(var(--lightgrey) l(+10%));
  background: ${lighten('0.1', colors.lightgrey)};
  border-radius: 100%;
  object-fit: cover;
`;

const ReadingTime = styled.span`
  flex-shrink: 0;
  margin-left: 20px;
  color: ${colors.midgrey};
  font-size: 1.2rem;
  line-height: 33px;
  font-weight: 500;
  letter-spacing: 0.5px;
  text-transform: uppercase;
`;

export interface PostCardProps {
  post: Post;
}

const PostCard: React.SFC<PostCardProps> = ({ post }) => {
  const postReadingTime = post.content && readingTime(post.content);
  return (
    <article className={`post-card ${PostCardStyles} ${!post.featured_media ? 'no-image' : ''}`}>
      {post.featured_media && (
        <Link className={`${PostCardImageLink} post-card-image-link`} to={post.slug}>
          <PostCardImage className="post-card-image">
            {post.featured_media &&
              post.featured_media.localFile &&
              post.featured_media.localFile.childImageSharp.fluid && (
                <Img
                  alt={`${post.title} cover image`}
                  style={{ height: '100%' }}
                  fluid={post.featured_media.localFile.childImageSharp.fluid}
                />
              )}
          </PostCardImage>
        </Link>
      )}
      <PostCardContent className="post-card-content">
        <div className={`${PostCardContentLink} post-card-content-link`}>
          <header className="post-card-header">
            <PostCardTitle>
              <Link to={post.slug}>{post.title} </Link>
            </PostCardTitle>
          </header>
          <PostCardExcerpt>
            <div dangerouslySetInnerHTML={{ __html: post.excerpt }} />
          </PostCardExcerpt>
        </div>
        <PostCardMeta className="post-card-meta">
          <AuthorList>
            <AuthorListItem>
              <AuthorNameTooltip className="author-name-tooltip">
                {post.author.name}
              </AuthorNameTooltip>
              <Link className={`${StaticAvatar}`} to={`/author/${post.author.slug}/`}>
                {post.author.acf &&
                post.author.acf.profilna_slika &&
                post.author.acf.profilna_slika.localFile ? (
                  <Img fluid={post.author.acf.profilna_slika.localFile.childImageSharp.fluid} />
                ) : (
                  <img
                    className={`${AuthorProfileImage}`}
                    src={post.author.avatar_urls.wordpress_48}
                    alt={post.author.name}
                  />
                )}
              </Link>
            </AuthorListItem>
          </AuthorList>
          {postReadingTime && <ReadingTime>{postReadingTime.text}</ReadingTime>}
        </PostCardMeta>
      </PostCardContent>
    </article>
  );
};

export default PostCard;
