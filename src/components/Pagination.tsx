import React, { StatelessComponent } from 'react';
import { Link } from 'gatsby';
import { SiteMain, outer, inner } from '../styles/shared';

interface Props {
  pageContext: {
    previousPagePath: string;
    nextPagePath: string;
  };
  pathPrefix?: string;
}
const Pagination: StatelessComponent<Props> = ({
  pageContext: { previousPagePath, nextPagePath },
}) => (
  <nav role="navigation" className={`${SiteMain} ${outer}`}>
    <div className={`navbar navbar-menu ${inner}`}>
      {previousPagePath && (
        <div className="navbar-item">
          <Link to={previousPagePath} rel="prev">
            Predhodna
          </Link>
        </div>
      )}
      {nextPagePath && (
        <div className="navbar-item">
          <Link to={nextPagePath} rel="next">
            Sledeća
          </Link>
        </div>
      )}
    </div>
  </nav>
);

Pagination.defaultProps = {};

export default Pagination;
