import React, { SFC } from 'react';
import Img from 'gatsby-image';
import styled from 'react-emotion';

interface Props {
  bgr?: { fluid: any };
  theme?: 'light' | 'dark';
  title: string;
}

const MainTitle = styled('h1')<{ theme: 'light' | 'dark' }>(({ theme }) => ({
  position: 'relative',
  zIndex: 3,
  textAlign: 'center',
  color: theme === 'dark' ? '#FFF' : '#212121',
}));

const Hgroup = styled.hgroup`
  position: relative;
  padding: 50px 20px;
  margin-bottom: 45px;
`;
export const BackgroundImage = styled(Img)`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const PageHgroup: SFC<Props> = ({ bgr, title, theme }) => (
  <Hgroup>
    {bgr && <BackgroundImage fluid={bgr.fluid} style={{ position: 'absolute' }} />}
    <MainTitle theme={theme}>{title}</MainTitle>
  </Hgroup>
);

PageHgroup.defaultProps = {
  theme: 'dark',
};
export default PageHgroup;
