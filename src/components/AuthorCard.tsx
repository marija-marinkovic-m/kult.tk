import { Link } from 'gatsby';
import Img from 'gatsby-image';
import React from 'react';
import styled from 'react-emotion';

import { colors } from '../styles/colors';
import { AuthorProfileImage } from '../styles/shared';
import { Author } from '../content/models';

const AuthorCardSection = styled.section`
  display: flex;
`;

const AuthorCardName = styled.h4`
  margin: 8px 0 2px 0;
  padding: 0;
  font-size: 2rem;

  a {
    color: ${colors.darkgrey};
    font-weight: 700;
  }

  a:hover {
    text-decoration: none;
  }
`;

const AuthorCardContent = styled.section`
  p {
    margin: 0;
    color: ${colors.midgrey};
    line-height: 1.3em;
  }
`;

export interface AuthorCardProps {
  author: Author;
}

const AuthorCard: React.SFC<AuthorCardProps> = ({ author }) => {
  return (
    <AuthorCardSection>
      {author.acf && author.acf.profilna_slika && author.acf.profilna_slika.localFile ? (
        <Img
          className={`${AuthorProfileImage}`}
          fluid={author.acf.profilna_slika.localFile.childImageSharp.fluid}
        />
      ) : (
        <img
          className={`${AuthorProfileImage}`}
          src={author.avatar_urls.wordpress_48}
          alt={author.id}
        />
      )}

      <AuthorCardContent>
        <AuthorCardName>
          <Link to={`/author/${author.slug}/`}>{author.name}</Link>
        </AuthorCardName>
        {author.description ? (
          <p>{author.description}</p>
        ) : (
          <p>
            Pregled <Link to={`/author/${author.slug}/`}>članaka</Link> ovog autora.
          </p>
        )}
      </AuthorCardContent>
    </AuthorCardSection>
  );
};

export default AuthorCard;
