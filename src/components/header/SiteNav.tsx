import { Link, StaticQuery, graphql } from 'gatsby';
import React from 'react';
import styled, { css } from 'react-emotion';

import { SocialAnchor } from '../../styles/shared';
import config from '../../website-config';
import Facebook from '../icons/facebook';
import Twitter from '../icons/twitter';
import SubscribeModal from '../subsribe/SubscribeOverlay';
import SiteNavLogo from './SiteNavLogo';
import { Category } from '../../content/models';

const HomeNavRaise = css`
  @media (min-width: 900px) {
    position: relative;
    top: -70px;
  }
`;

const SiteNavStyles = css`
  position: relative;
  z-index: 300;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  overflow-y: hidden;
  height: 40px;
  font-size: 1.2rem;
`;

const SiteNavLeft = styled.div`
  display: flex;
  align-items: center;
  overflow-x: auto;
  overflow-y: hidden;
  -webkit-overflow-scrolling: touch;
  margin-right: 10px;
  padding-bottom: 80px;
  letter-spacing: 0.4px;
  white-space: nowrap;

  -ms-overflow-scrolling: touch;

  @media (max-width: 700px) {
    margin-right: 0;
    padding-left: 4vw;
  }
`;

const NavStyles = ({ theme = 'light' }) => css`
  display: flex;
  margin: 0 0 0 -12px;
  padding: 0;
  list-style: none;
  filter: drop-shadow(0 0 1.2rem rgba(0, 0, 0, 0.35));

  li {
    display: block;
    margin: 0;
    padding: 0;
    text-transform: uppercase;
  }

  li a {
    display: block;
    margin: 0;
    padding: 10px 12px;
    color: ${theme === 'light' ? '#fff' : '#000'};
    opacity: 0.8;
  }

  li a:hover {
    text-decoration: none;
    opacity: 1;
  }
`;

const NavUl = styled.ul<{ theme: string }>(NavStyles);

const SiteNavRight = styled.div`
  flex-shrink: 0;
  display: flex;
  align-items: center;
  height: 40px;

  @media (max-width: 700px) {
    display: none;
  }
`;

const SocialLinks = styled.div`
  flex-shrink: 0;
  display: flex;
  align-items: center;
  a:last-of-type {
    padding-right: 20px;
  }
`;

const SubscribeButton = styled.a<{ theme: string }>(({ theme }) => ({
  display: 'block',
  padding: '4px 10px',
  border: `${theme === 'light' ? '#fff' : '#000'} 1px solid`,
  color: theme === 'light' ? '#fff' : '#000',
  fontSize: '1.2rem',
  lineHeight: '1em',
  borderRadius: '10px',
  opacity: 0.8,

  '&:hover': {
    textDecoration: 'none',
    opacity: 1,
    cursor: 'pointer',
  },
}));

interface SiteNavProps {
  isHome?: boolean;
  theme: 'dark' | 'light';
}

interface SiteNaveState {
  isOpen: boolean;
}

class SiteNav extends React.Component<SiteNavProps, SiteNaveState> {
  static defaultProps = {
    isHome: false,
    theme: 'light',
  };
  subscribe = React.createRef<SubscribeModal>();

  constructor(props: SiteNavProps) {
    super(props);
    this.state = { isOpen: false };
  }
  openModal = () => {
    if (this.subscribe.current) {
      this.subscribe.current.open();
    }
  };

  render() {
    const { isHome, theme } = this.props;
    return (
      <nav className={`${isHome ? HomeNavRaise : ''} ${SiteNavStyles}`}>
        <StaticQuery
          query={graphql`
            query {
              allWordpressCategory(
                filter: { parent_element: { id: { eq: null } }, wordpress_id: { ne: 1 } }
              ) {
                edges {
                  node {
                    name
                    slug
                    id
                  }
                }
              }
            }
          `}
          render={({ allWordpressCategory }) => (
            <SiteNavLeft>
              {!isHome && <SiteNavLogo />}
              <NavUl theme={theme} role="menu">
                {/* TODO: mark current nav item - add class nav-current */}
                {allWordpressCategory.edges.map((props: { node: Category }) => (
                  <li key={props.node.id} role="menuitem">
                    <Link to={`/categories/${props.node.slug}`}>{props.node.name}</Link>
                  </li>
                ))}
              </NavUl>
            </SiteNavLeft>
          )}
        />

        <SiteNavRight>
          <SocialLinks>
            {config.facebook && (
              <SocialAnchor
                theme={theme}
                href={config.facebook}
                target="_blank"
                title="Facebook"
                rel="noopener noreferrer"
              >
                <Facebook />
              </SocialAnchor>
            )}
            {config.twitter && (
              <SocialAnchor
                theme={theme}
                href={config.twitter}
                title="Twitter"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Twitter />
              </SocialAnchor>
            )}
          </SocialLinks>
          {config.showSubscribe && (
            <SubscribeButton theme={theme} onClick={this.openModal}>
              Prijavi se
            </SubscribeButton>
          )}
          {config.showSubscribe && <SubscribeModal ref={this.subscribe} />}
        </SiteNavRight>
      </nav>
    );
  }
}

export default SiteNav;
