import { darken, desaturate, lighten, mix } from 'polished';
import React, { Component } from 'react';
import addToMailchimp from 'gatsby-plugin-mailchimp';
import { css, default as styled } from 'react-emotion';

import { colors } from '../../styles/colors';

interface Props {}
interface State {
  email: string;
  sending: boolean;
  status: { result: string; msg: string };
}

export const Alert = styled('div')<{ error: boolean }>(({ error = false }) => ({
  padding: '15px',
  margin: '15px auto !important',
  borderRadius: '4px',
  lineHeight: 1.5,
  fontSize: '14px',
  boxSizing: 'border-box',
  border: error ? '1px solid #ffa39e' : '1px solid #b7eb8f',
  backgroundColor: error ? '#fff1f0' : '#f6ffed',
  color: error ? '#ffa39e' : '#b7eb8f',
  a: {
    color: '#ff008d',
  },
}));

export const Loader = ({ fill = '#333', style = {} }: { fill: string; style?: {} }) => (
  <svg
    version="1.1"
    x="0px"
    y="0px"
    width="24px"
    height="30px"
    viewBox="0 0 24 30"
    style={style}
    // style={{ enableBackground: 'new 0 0 50 50', ...style }}
  >
    <rect x="0" y="13" width="4" height="5" fill={fill}>
      <animate
        attributeName="height"
        attributeType="XML"
        values="5;21;5"
        begin="0s"
        dur="0.6s"
        repeatCount="indefinite"
      />
      <animate
        attributeName="y"
        attributeType="XML"
        values="13; 5; 13"
        begin="0s"
        dur="0.6s"
        repeatCount="indefinite"
      />
    </rect>
    <rect x="10" y="13" width="4" height="5" fill={fill}>
      <animate
        attributeName="height"
        attributeType="XML"
        values="5;21;5"
        begin="0.15s"
        dur="0.6s"
        repeatCount="indefinite"
      />
      <animate
        attributeName="y"
        attributeType="XML"
        values="13; 5; 13"
        begin="0.15s"
        dur="0.6s"
        repeatCount="indefinite"
      />
    </rect>
    <rect x="20" y="13" width="4" height="5" fill={fill}>
      <animate
        attributeName="height"
        attributeType="XML"
        values="5;21;5"
        begin="0.3s"
        dur="0.6s"
        repeatCount="indefinite"
      />
      <animate
        attributeName="y"
        attributeType="XML"
        values="13; 5; 13"
        begin="0.3s"
        dur="0.6s"
        repeatCount="indefinite"
      />
    </rect>
  </svg>
);

const SubscribeFormStyles = css`
  @media (max-width: 500px) {
    -ms-flex-direction: column;
    flex-direction: column;
  }
`;

const SubscribeEmail = styled.input`
  display: block;
  padding: 10px;
  width: 100%;
  /* border: color(var(--lightgrey) l(+7%)) 1px solid; */
  border: ${lighten('0.07', colors.lightgrey)};
  color: ${colors.midgrey};
  font-size: 1.8rem;
  line-height: 1em;
  font-weight: normal;
  user-select: text;
  border-radius: 5px;
  transition: border-color 0.15s linear;

  -webkit-appearance: none;
  :focus {
    outline: 0;
    /* border-color: color(var(--lightgrey) l(-2%)); */
    border-color: ${darken('0.02', colors.lightgrey)};
  }
`;

const SubscribeFormButton = styled.button`
  display: inline-block;
  margin: 0 0 0 10px;
  padding: 0 20px;
  height: 41px;
  outline: none;
  color: #fff;
  font-size: 1.5rem;
  line-height: 37px;
  font-weight: 400;
  text-align: center;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1);
  /* background: linear-gradient(
    color(var(--blue) whiteness(+7%)),
    color(var(--blue) lightness(-7%) saturation(-10%)) 60%,
    color(var(--blue) lightness(-7%) saturation(-10%)) 90%,
    color(var(--blue) lightness(-4%) saturation(-10%))
  ); */
  background: linear-gradient(
    ${mix('0.1', '#fff', colors.blue)},
    ${desaturate('0.1', darken('0.07', colors.blue))} 60%,
    ${desaturate('0.1', darken('0.07', colors.blue))} 90%,
    ${desaturate('0.1', darken('0.04', colors.blue))}
  );
  border-radius: 5px;
  box-shadow: 0 0 0 1px inset rgba(0, 0, 0, 0.14);

  -webkit-font-smoothing: subpixel-antialiased;

  :active,
  :focus {
    /* background: color(var(--blue) lightness(-9%) saturation(-10%)); */
    background: ${desaturate('0.1', darken('0.09', colors.blue))};
  }
  @media (max-width: 500px) {
    margin: 10px 0 0;
    width: 100%;
  }
`;

const FormGroup = styled.div`
  flex-grow: 1;
  @media (max-width: 500px) {
    width: 100%;
  }
`;
export default class SubscribeForm extends Component<Props, State> {
  state = {
    email: '',
    sending: false,
    status: { msg: '', result: '' },
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ email: e.target.value });
  };
  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.setState({ sending: true, status: { msg: '', result: '' } });
    const { email } = this.state;

    addToMailchimp(email).then((status: { result: string; msg: string }) => {
      if (status.result !== 'error') {
        this.setState({ email: '' });
      }
      this.setState({
        sending: false,
        status,
      });
    });
  };

  render() {
    const {
      status: { msg, result },
      sending,
    } = this.state;
    return (
      <div>
        <form
          className={`${SubscribeFormStyles}`}
          onSubmit={this.handleSubmit}
          id="mc-embedded-subscribe-form"
          name="mc-embedded-subscribe-form"
          target="_blank"
          noValidate
        >
          <FormGroup className="form-group">
            <SubscribeEmail
              className="subscribe-email"
              type="email"
              name="email"
              onChange={this.handleChange}
              // placeholder="tvojemail@primer.com"
            />
          </FormGroup>
          <SubscribeFormButton type="submit">
            {sending ? (
              <Loader
                fill="#FFF"
                style={{ position: 'relative', top: 7, enableBackground: 'new 0 0 50 50' }}
              />
            ) : (
              <span>Prijavi se!</span>
            )}
          </SubscribeFormButton>
        </form>

        {msg && (
          <Alert
            className="alert"
            error={result === 'error'}
            dangerouslySetInnerHTML={{ __html: msg }}
          />
        )}
      </div>
    );
  }
}
