import React, { Component } from 'react';
import { graphql } from 'gatsby';

import { Post } from '../content/models';
import PostCard from './PostCard';
import { SiteMain, outer, inner, PostFeed, PostFeedRaise } from '../styles/shared';

interface Props {
  posts: { node: Post }[];
  title?: string;
}

export default class PostList extends Component<Props, any> {
  render() {
    const { posts, children } = this.props;

    return (
      <main className={`${SiteMain} ${outer}`}>
        <div className={inner}>
          <div className={`${PostFeed} ${PostFeedRaise}`}>
            {posts.map(({ node: post }) => (
              <PostCard key={post.id} post={post} />
            ))}
          </div>
        </div>

        {children}
      </main>
    );
  }
}

export const query = graphql`
  fragment PostListFields on wordpress__POST {
    id
    title
    excerpt
    slug
    content
    tags {
      id
      slug
      name
      count
    }
    categories {
      id
      name
      slug
      count
    }
    author {
      id
      name
      slug
      description
      avatar_urls {
        wordpress_48
      }
      acf {
        profilna_slika {
          localFile {
            childImageSharp {
              fluid(maxWidth: 48) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
    featured_media {
      localFile {
        childImageSharp {
          fluid(maxWidth: 1920) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
    modified(formatString: "MMMM DD, YYYY")
  }
`;
