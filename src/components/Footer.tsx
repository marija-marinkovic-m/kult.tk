import { Link, StaticQuery, graphql } from 'gatsby';
import { setLightness } from 'polished';
import React from 'react';
import styled, { css } from 'react-emotion';

import { colors } from '../styles/colors';
import { outer, inner } from '../styles/shared';
import config from '../website-config';
import { Post } from '../content/models';

const SiteFooter = css`
  position: relative;
  padding-top: 20px;
  padding-bottom: 60px;
  color: #fff;
  background: ${setLightness('0.0015', colors.darkgrey)};
`;

const SiteFooterContent = css`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  color: rgba(255, 255, 255, 0.7);
  font-size: 1.3rem;
  a {
    color: rgba(255, 255, 255, 0.7);
  }
  a:hover {
    color: rgba(255, 255, 255, 1);
    text-decoration: none;
  }
  @media (max-width: 650px) {
    flex-direction: column;
  }
`;

const SiteFooterNav = styled.nav`
  display: flex;

  a {
    position: relative;
    margin-left: 20px;
  }

  a:before {
    content: '';
    position: absolute;
    top: 11px;
    left: -11px;
    display: block;
    width: 2px;
    height: 2px;
    background: #fff;
    border-radius: 100%;
  }

  a:first-of-type:before {
    display: none;
  }
  @media (max-width: 650px) {
    a:first-child {
      margin-left: 0;
    }
  }
`;

const Footer: React.SFC = () => {
  return (
    <footer className={`${outer} ${SiteFooter}`}>
      <div className={`${inner} ${SiteFooterContent}`}>
        <section className="copyright">
          <Link to="/">{config.title}</Link> &copy; {new Date().getFullYear()}
        </section>
        <SiteFooterNav>
          <Link to="/">Početna</Link>
          <StaticQuery
            query={graphql`
              query {
                allWordpressPage(sort: { fields: menu_order }) {
                  edges {
                    node {
                      slug
                      id
                      title
                    }
                  }
                }
              }
            `}
            render={({ allWordpressPage }) =>
              allWordpressPage.edges.map((props: { node: Post }) => (
                <Link key={props.node.id} to={props.node.slug}>
                  {props.node.title}
                </Link>
              ))
            }
          />
          {config.facebook && (
            <a href={config.facebook} target="_blank" rel="noopener noreferrer">
              Facebook
            </a>
          )}
          {config.twitter && (
            <a href={config.twitter} target="_blank" rel="noopener noreferrer">
              Twitter
            </a>
          )}

          {/* <a href="https://ghost.org" target="_blank" rel="noopener noreferrer">
            Kult
          </a> */}

          <Link to="/rss.xml">RSS</Link>
        </SiteFooterNav>
      </div>

      <div id="modal-root" />
    </footer>
  );
};

export default Footer;
