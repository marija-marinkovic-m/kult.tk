import React, { SFC } from 'react';
import styled from 'react-emotion';

import { SiteHeader, outer, inner } from '../styles/shared';
import SiteNav from '../components/header/SiteNav';
import Footer from '../components/Footer';

interface Props {}

const Wrapper = styled.div`
  position: relative;
  .site-main {
    background: #fff;
    padding-bottom: 4vw;
  }
`;
const Placeholder = styled.div`
  background: #fff;
  padding-bottom: 4vw;
`;

const MainLayout: SFC<Props> = ({ children }) => (
  <Wrapper>
    <header className={`${SiteHeader} ${outer}`}>
      <div className={inner}>
        <SiteNav />
      </div>
    </header>
    <Placeholder>{children}</Placeholder>
    <Footer />
  </Wrapper>
);

MainLayout.defaultProps = {};
export default MainLayout;
