import { graphql, Link } from 'gatsby';
import * as React from 'react';
import styled, { css } from 'react-emotion';

import SiteNavLogo from '../components/header/SiteNavLogo';
import PostCard from '../components/PostCard';
import Wrapper from '../components/Wrapper';
import IndexLayout from '../layouts';
import { colors } from '../styles/colors';
import { inner, outer, PostFeed, SiteHeader } from '../styles/shared';
import { Post } from '../content/models';

const SiteNavCenter = styled.nav`
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;

  .site-nav-logo {
    margin-right: 0;
  }
`;

const ErrorTemplate = css`
  padding: 7vw 4vw;
`;

const ErrorCode = styled.h1`
  margin: 0;
  font-size: 12vw;
  line-height: 1em;
  letter-spacing: -5px;
  opacity: 0.3;
`;

const ErrorDescription = styled.p`
  margin: 0;
  color: ${colors.midgrey};
  font-size: 3rem;
  line-height: 1.3em;
  font-weight: 400;
`;

const ErrorLink = css`
  display: inline-block;
  margin-top: 5px;
`;

interface NotFoundTemplateProps {
  data: {
    allWordpressPost: {
      totalCount: number;
      edges: {
        node: Post;
      }[];
    };
  };
}

const NotFoundPage: React.SFC<NotFoundTemplateProps> = props => {
  const { edges } = props.data.allWordpressPost;

  return (
    <IndexLayout>
      <Wrapper>
        <header className={`${SiteHeader} ${outer}`}>
          <div className="inner">
            <SiteNavCenter>
              <SiteNavLogo />
            </SiteNavCenter>
          </div>
        </header>
        <main id="site-main" className={`${ErrorTemplate} ${outer}`}>
          <div className={`${inner}`}>
            <section style={{ textAlign: 'center' }}>
              <ErrorCode>404</ErrorCode>
              <ErrorDescription>Stranica nije pronađena</ErrorDescription>
              <Link className={`${ErrorLink}`} to={''}>
                Vrati se na početnu →
              </Link>
            </section>
          </div>
        </main>
        <aside className={`${outer}`}>
          <div className={`${inner}`}>
            <div className={`${PostFeed}`}>
              {edges.map(({ node }) => (
                <PostCard key={node.slug} post={node} />
              ))}
            </div>
          </div>
        </aside>
      </Wrapper>
    </IndexLayout>
  );
};

export default NotFoundPage;

export const pageQuery = graphql`
  query {
    allWordpressPost(limit: 3) {
      edges {
        node {
          title
          id
          date
          slug
          excerpt
          modified(formatString: "MMMM DD")
          author {
            id
            name
            slug
            avatar_urls {
              wordpress_48
            }
            acf {
              profilna_slika {
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 48) {
                      ...GatsbyImageSharpFluid_withWebp
                    }
                  }
                }
              }
            }
          }
          template
          featured_media {
            id
            source_url
            localFile {
              childImageSharp {
                fluid(maxWidth: 500) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`;
