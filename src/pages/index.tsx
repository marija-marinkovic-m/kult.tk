import { graphql } from 'gatsby';
import React from 'react';
import { css } from 'react-emotion';
import Helmet from 'react-helmet';
import Img from 'gatsby-image';

import Particles from 'react-particles-js';

import Footer from '../components/Footer';
import SiteNav from '../components/header/SiteNav';
import Wrapper from '../components/Wrapper';
import IndexLayout from '../layouts';
import config from '../website-config';
import {
  inner,
  outer,
  SiteDescription,
  SiteHeader,
  SiteHeaderContent,
  SiteTitle,
} from '../styles/shared';

import { Post } from '../content/models';
import Pagination from '../components/Pagination';
import PostList from '../components/PostList';

const HomePosts = css`
  @media (min-width: 795px) {
    .post-card:first-child {
      flex: 1 1 100% !important;
      .post-card-image-link {
        display: none !important;
      }
      .post-card-content {
        flex: 1 1 100% !important;
      }
    }
    .post-card:nth-child(6n + 1):not(.no-image) {
      flex: 1 1 100%;
      flex-direction: row;
    }

    .post-card:nth-child(6n + 1):not(.no-image) .post-card-image-link {
      position: relative;
      flex: 1 1 auto;
      border-radius: 5px 0 0 5px;
    }

    .post-card:nth-child(6n + 1):not(.no-image) .post-card-image {
      position: absolute;
      width: 100%;
      height: 100%;
    }

    .post-card:nth-child(6n + 1):not(.no-image) .post-card-content {
      flex: 0 1 357px;
    }

    .post-card:nth-child(6n + 1):not(.no-image) h2 {
      font-size: 2.6rem;
    }

    .post-card:nth-child(6n + 1):not(.no-image) p {
      font-size: 1.8rem;
      line-height: 1.55em;
    }

    .post-card:nth-child(6n + 1):not(.no-image) .post-card-content-link {
      padding: 30px 40px 0;
    }

    .post-card:nth-child(6n + 1):not(.no-image) .post-card-meta {
      padding: 0 40px 30px;
    }
  }
`;

export interface IndexProps {
  data: {
    logo: {
      childImageSharp: {
        fixed: any;
      };
    };
    logowhite: {
      childImageSharp: {
        fixed: any;
      };
    };
    header: {
      childImageSharp: {
        fluid: any;
      };
    };
    allWordpressPost: {
      edges: {
        node: Post;
      }[];
    };
    wordpressWpUsers: {
      acf: {
        theme: boolean;
        image_url: {
          localFile: {
            childImageSharp: {
              fluid: any;
            };
          };
        };
      };
    };
  };
  pageContext: {
    currentPage: number;
    numPages: number;
    previousPagePath: string;
    nextPagePath: string;
  };
}

const IndexPage: React.SFC<IndexProps> = props => {
  let heroImage = props.data.header.childImageSharp.fluid;
  let heroWhiteTheme = false;
  if (props.data.wordpressWpUsers && props.data.wordpressWpUsers.acf) {
    if (props.data.wordpressWpUsers.acf.theme) {
      heroWhiteTheme = true;
    }
    if (props.data.wordpressWpUsers.acf.image_url) {
      heroImage = props.data.wordpressWpUsers.acf.image_url.localFile.childImageSharp.fluid;
    }
  }
  const width = heroImage.sizes.split(', ')[1].split('px')[0];
  const height = String(Number(width) / heroImage.aspectRatio);
  return (
    <IndexLayout className={`${HomePosts}`}>
      <Helmet>
        <title>
          {config.title} - {config.description}
        </title>
        <meta property="og:site_name" content={config.title} />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={config.title} />
        <meta property="og:description" content={config.description} />
        <meta property="og:url" content={config.siteUrl} />
        <meta property="og:image" content={heroImage.src} />
        <meta property="article:publisher" content={config.facebook} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content={config.title} />
        <meta name="twitter:description" content={config.description} />
        <meta name="twitter:url" content={config.siteUrl} />
        <meta name="twitter:image" content={heroImage.src} />
        {config.twitter && (
          <meta
            name="twitter:site"
            content={`@${config.twitter.split('https://twitter.com/')[0]}`}
          />
        )}
        <meta property="og:image:width" content={width} />
        <meta property="og:image:height" content={height} />
      </Helmet>
      <Wrapper>
        <header
          className={`${SiteHeader} ${outer}`}
          style={{
            backgroundImage: `url('${heroImage.src}')`,
          }}
        >
          <Particles
            style={{ position: 'absolute' }}
            params={{
              particles: {
                number: {
                  value: 160,
                  density: {
                    enable: false,
                  },
                },
                size: {
                  value: 3,
                  random: true,
                  anim: {
                    speed: 4,
                    size_min: 0.3,
                  },
                },
                line_linked: {
                  enable: false,
                },
                move: {
                  random: true,
                  speed: 1,
                  direction: 'top',
                  out_mode: 'out',
                },
              },
              interactivity: {
                events: {
                  onhover: {
                    enable: true,
                    mode: 'bubble',
                  },
                  onclick: {
                    enable: true,
                    mode: 'repulse',
                  },
                },
                modes: {
                  bubble: {
                    distance: 250,
                    duration: 2,
                    size: 0,
                    opacity: 0,
                  },
                  repulse: {
                    distance: 400,
                    duration: 4,
                  },
                },
              },
            }}
          />

          <div className={`${inner}`}>
            <SiteHeaderContent>
              <SiteTitle>
                {props.data.logo ? (
                  <Img
                    fixed={props.data[heroWhiteTheme ? 'logowhite' : 'logo'].childImageSharp.fixed}
                    alt={config.title}
                  />
                ) : (
                  config.title
                )}
              </SiteTitle>
              <SiteDescription theme={heroWhiteTheme ? 'light' : 'dark'}>
                {config.description}
              </SiteDescription>
            </SiteHeaderContent>
            <SiteNav isHome={true} theme={heroWhiteTheme ? 'light' : 'dark'} />
          </div>
        </header>

        <PostList posts={props.data.allWordpressPost.edges}>
          <Pagination pageContext={props.pageContext} />
        </PostList>

        {props.children}

        <Footer />
      </Wrapper>
    </IndexLayout>
  );
};

export default IndexPage;

export const pageQuery = graphql`
  query {
    logo: file(relativePath: { eq: "img/kult.png" }) {
      childImageSharp {
        fixed(width: 210) {
          ...GatsbyImageSharpFixed_tracedSVG
        }
      }
    }
    logowhite: file(relativePath: { eq: "img/kult-white.png" }) {
      childImageSharp {
        fixed(width: 210) {
          ...GatsbyImageSharpFixed_tracedSVG
        }
      }
    }
    header: file(relativePath: { eq: "img/speeches-cover.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 2000) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    wordpressWpUsers(wordpress_id: { eq: 3 }) {
      acf {
        theme
        image_url {
          localFile {
            childImageSharp {
              fluid(maxWidth: 2000) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
    allWordpressPost {
      edges {
        node {
          ...PostListFields
        }
      }
    }
  }
`;
